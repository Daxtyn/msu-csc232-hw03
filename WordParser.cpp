/*
 * @file WordParser.cpp
 * @authors James R. Daehn and Daxtyn Hiestand
 * @brief Implementation of WordParser class.
 */

#include "WordParser.h"

bool WordParser::isWord(std::string s) {
 //Base case, checks that length of one is always a dot.
  if (s.length() == 1){
		if (s[0] == DOT)
			return true;
		else
			return false;
	 }
		
	else if (s.length() > 1){
		if (s[0] == DASH)
			//first character strip
			return isWord( s.substr( 1, s.length()));
	
		
		else if (s[s.length() -1] == DOT)
			//last character strip
			return isWord (s.substr(0, s.length() -1));
			
	}
	
	
}
